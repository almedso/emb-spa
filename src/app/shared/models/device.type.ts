export interface DeviceType {
    host: string;
    token: string;
    name: string;
  }
