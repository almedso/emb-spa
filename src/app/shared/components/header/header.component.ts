import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() showHomeNavigation = true;
  @Input() icon =  '';
  @Input() title = 'ESP Simulator';

  constructor() { }

  ngOnInit(): void {
  }

}
