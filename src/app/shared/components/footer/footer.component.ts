import { Component, OnInit } from '@angular/core';
import { buildInfo } from 'src/build';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public version: string;
  public gitShaOne: string;

  constructor() {
  }

  ngOnInit(): void {
    this.version = buildInfo.version;
    this.gitShaOne = buildInfo.hash;
  }

}
