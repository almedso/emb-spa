import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

import { CommonModule } from '@angular/common';

import { AuthService } from './services/auth.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { DeviceService } from './services/device.service';
import { DeviceServiceConfig } from './services/device.service';
import { HttpMockRequestInterceptor } from './mocks/simulator.mock';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  imports:      [ CommonModule ],
//  declarations: [ TitleComponent ],
//  exports:      [ TitleComponent ],
  providers:    [
    DeviceService,
    AuthService,
    AuthGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpMockRequestInterceptor,
      multi: true
    },
 ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(config: DeviceServiceConfig): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [ {provide: DeviceServiceConfig, useValue: config }]
    };
  }
}
