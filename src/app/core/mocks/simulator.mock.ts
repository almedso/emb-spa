import { Injectable, Injector } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';


const httpHeaders = new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': [ 'POST' ],
});

const urls = [
    {
        url: 'http://127.0.0.1/authenticate',
        json: {
            host: '127.0.0.1',
            name: 'Simulator Backend',
            token: 123456,
        },
        status: 200,
        delayInMilliSec: 500
    },
    {
        url: 'http://127.0.0.2/authenticate',
        json: {
            error: 'Wrong token simulator',
        },
        status: 403,
        delayInMilliSec: 500

    },
    {
        url: 'http://127.0.0.3/authenticate',
        json: {
            error: 'Server Error',
        },
        status: 501,
        delayInMilliSec: 500
    },
];

@Injectable()
export class HttpMockRequestInterceptor implements HttpInterceptor {
    constructor(private injector: Injector) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('Intercepted httpCall: ' + request.url);
        for (const element of urls) {
            if (request.url === element.url) {
                console.log('Intercepted Request URL: ' + request.url);
                console.log('Intercepted Response Status: ' + element.status.toString());
                console.log('Intercepted Response: ' + JSON.stringify(element.json));
                console.log('Intercepted Simulated Delay [ms]: ' + JSON.stringify(element.delayInMilliSec));
                if (element.status === 200) {
                  return of(new HttpResponse({
                    body: (element.json) as any,
                    status: element.status,
                    headers: httpHeaders,  // this is to setup CORS correctly
                  })).pipe(delay(element.delayInMilliSec));
                } else {
                  throw new HttpErrorResponse({
                    error: element.json.error,
                    headers: httpHeaders,
                    status: element.status,
                    statusText: 'ERROR',
                    url: element.url
                  });
                }
            }
        }
        return next.handle(request);
    }
}
