import { Injectable, Optional } from '@angular/core';

import { DeviceType } from '@/shared';

// Proves that UserService is an app-wide singleton and only instantiated once
// IFF shared.module follows the `forRoot` pattern.
//
// If it didn't, a new instance of UserService would be created
// after each lazy load and the userName would double up.

interface DeviceTypeTimestamped extends DeviceType {
  timestamp: number; // number of milliseconds since 1970-01-01 00:00:00
}


export class DeviceServiceConfig {
  defaultDevices: DeviceType[] = [
    // we deliberately choose the IP address of localhost for
    // simulation per mock. Since it cannot be used otherhow
    {name: 'Simulate Backend', host: '127.0.0.1', token: '123456' },
  ];
}


const STORAGE_KEY = 'emb-spa-devices';
const MAX_DEVICES = 10;
const MAX_STORAGE_MILLISEC = 7 * 24 * 3600 * 1000; // 7 Tage

///
/// The device service always maintains maximum x devices
/// if there are more than those the oldest accessed device is removed
/// host id is unique access key other values are overwritten
///
@Injectable()
export class DeviceService {

  devices: DeviceTypeTimestamped[] = [];

  constructor(@Optional() config: DeviceServiceConfig ) {
    if ( config ) {
      this.devices = config.defaultDevices.map( x => {
        // add current timestamp
        return {
          name: x.name,
          host: x.host,
          token: x.token,
          timestamp: new Date().getTime(),
        };
      });
    }

    const devicesAsString = localStorage.getItem(STORAGE_KEY);
    if ( devicesAsString ) {
      this.devices = JSON.parse(devicesAsString) as DeviceTypeTimestamped[];
    }
    console.log('DeviceService c-tor - loaded: ', devicesAsString, this.devices);
  }

  /// write devices to local storage
  _updateDevices(): void {
    const devicesAsString = JSON.stringify(this.devices);
    localStorage.setItem(STORAGE_KEY, devicesAsString);
    console.log('DeviceService - update local storage');
  }

  /// sort the devices based on last access
  _sortDevices(): void {
    const devices = this.devices.slice(0);
    this.devices = devices.sort( (x, y: DeviceTypeTimestamped) => {
      return y.timestamp - x.timestamp;
    });
  }

  /// remove devices that that either too old or
  /// too much
  /// Assumption: the devices are sorted.
  _pruneDevices(): void {
    const oldest = new Date().getTime() -  MAX_STORAGE_MILLISEC;
    let index: number;
    for (index = 0; index < this.devices.length; index++ ) {
      if (this.devices[index].timestamp  < oldest) {
        break;
      }
    }
    if ( index > MAX_DEVICES) { index = MAX_DEVICES ; }
    this.devices = this.devices.slice(0, index);
  }

  getDevices(): DeviceType[] {
    console.log(this.devices);
    return this.devices.map( td => {
      return { name: td.name, host: td.host, token: td.token };
    });
  }

  /**
   * Add or update a device
   *
   * @param device The device to add or update
   *
   * the host part of the device is primary key.
   *  i.e. is used to decide to add or update.
   */
  addDevice(device: DeviceType): void {
    let updated = false;
    let index: number;
    const newDevice = {
      name: device.name,
      token: device.token,
      host: device.host,
      timestamp: new Date().getTime(),
    };
    for (index = 0; index < this.devices.length; index++ ) {
      if (this.devices[index].host === device.host) {
        updated = true;
        break;
      }
    }
    if (updated) {
      this.devices.splice(index, 1, newDevice);
    } else {
      // append as new device
      this.devices.push(newDevice);
    }
    // sort, prune and store devices
    this._sortDevices();
    this._pruneDevices();
    this._updateDevices();
  }

  removeDevices(): void {
    this.devices = [];
    localStorage.removeItem(STORAGE_KEY);
  }
}
