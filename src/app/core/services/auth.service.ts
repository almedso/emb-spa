import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { DeviceType } from '@/shared';

const httpOptions = {
  headers: new HttpHeaders({
    Authorization: 'authkey',
  })
};

@Injectable({ providedIn: 'root' })
export class AuthService {
    private currentDeviceSubject: BehaviorSubject<DeviceType>;
    public currentDevice: Observable<DeviceType>;

    constructor(private http: HttpClient) {
        this.currentDeviceSubject = new BehaviorSubject<DeviceType>(JSON.parse(localStorage.getItem('currentDevice')));
        this.currentDevice = this.currentDeviceSubject.asObservable();
    }

    public get currentDeviceValue(): DeviceType {
        return this.currentDeviceSubject.value;
    }

    /// login to device
    /// if local host is provided alway successful
    /// otherwise alternating (for now)
    /// returns asynchronously
    login(host: string, token: string) {
      return this.http.post<DeviceType>('http://' + host + '/authenticate', token, httpOptions)
        .pipe(
          map( (device: DeviceType) => {
            // login successful if there's a token in the response
            if (device) {
              console.log('Successful login: ', device);
              // store device details local storage to keep device
              // logged in between page refreshes
              localStorage.setItem('currentDevice', JSON.stringify(device));
              this.currentDeviceSubject.next(device);
            }
            return device;
          }),
          catchError( error => {
            if (error.error instanceof ErrorEvent) {
              // client-side error
              console.error( `BrowserError: ${error.error.message}`);
              // 900 is a proprietary error - not standardized -
              // we use it to transport client side errors
              return throwError({status: 900, message: error.error.message });
            } else {
              // server-side error
              console.error(`Error Code: ${error.status}\nMessage: ${error.message}`);
              return throwError(error);
            }
          }) // catchError
        ); // pipe
    }

    logout() {
        // remove device from local storage to log device out
        localStorage.removeItem('currentDevice');
        this.currentDeviceSubject.next(null);
    }

    isAuthenticated(): boolean {
      return (localStorage.getItem('currentDevice') !== null);
    }
}
