import { TestBed } from '@angular/core/testing';

import { DeviceService } from './device.service';
import { CoreModule } from '@/core/core.module';
import { DeviceType } from '@/shared/models/device.type';


function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}

describe('DeviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      CoreModule
    ],
  }));

  afterEach(() => {
    const service: DeviceService = TestBed.inject(DeviceService);
    service.removeDevices();
  });

  it('should be created', () => {
    const service: DeviceService = TestBed.inject(DeviceService);
    expect(service).toBeTruthy();
  });

  it('should contain initially nothing', () => {
    const service: DeviceService = TestBed.inject(DeviceService);
    const devices: DeviceType[] = service.getDevices();
    expect(devices).toEqual([]);
  });


  it('should contain one device after inserting', () => {
    const service: DeviceService = TestBed.inject(DeviceService);
    service.addDevice({ name: 'Simulate Backend', host: '127.0.0.1', token: '123456'});
    const devices: DeviceType[] = service.getDevices();
    expect(devices).toEqual([{ name: 'Simulate Backend', host: '127.0.0.1', token: '123456'}]);
  });

  it('should contain one device after altering', () => {
    const service: DeviceService = TestBed.inject(DeviceService);
    service.addDevice({ name: 'Simulate Backend', host: '127.0.0.1', token: '123456'});
    service.addDevice({ name: 'Simulate Backend 2', host: '127.0.0.1', token: '666666'});
    const devices: DeviceType[] = service.getDevices();
    expect(devices).toEqual([{ name: 'Simulate Backend 2', host: '127.0.0.1', token: '666666'}]);
  });

  it('should contain three sorted devices after inserting', async () => {
    const service: DeviceService = TestBed.inject(DeviceService);
    service.addDevice({ name: 'Simulate Backend 1', host: '127.0.0.1', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 2', host: '127.0.0.2', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 3', host: '127.0.0.3', token: '123456'});
    const devices: DeviceType[] = service.getDevices();
    expect(devices).toEqual([
      { name: 'Simulate Backend 3', host: '127.0.0.3', token: '123456'},
      { name: 'Simulate Backend 2', host: '127.0.0.2', token: '123456'},
      { name: 'Simulate Backend 1', host: '127.0.0.1', token: '123456'},
    ]);
  });

  it('should contain three sorted devices after altering', async () => {
    const service: DeviceService = TestBed.inject(DeviceService);
    service.addDevice({ name: 'Simulate Backend 1', host: '127.0.0.1', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 2', host: '127.0.0.2', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 3', host: '127.0.0.3', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 2', host: '127.0.0.2', token: '666666'});
    const devices: DeviceType[] = service.getDevices();
    expect(devices).toEqual([
      { name: 'Simulate Backend 2', host: '127.0.0.2', token: '666666'},
      { name: 'Simulate Backend 3', host: '127.0.0.3', token: '123456'},
      { name: 'Simulate Backend 1', host: '127.0.0.1', token: '123456'},
    ]);
  });

  it('should contain ten sorted devices after inserting 11', async () => {
    const service: DeviceService = TestBed.inject(DeviceService);
    service.addDevice({ name: 'Simulate Backend 1', host: '127.0.0.1', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 2', host: '127.0.0.2', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 3', host: '127.0.0.3', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 4', host: '127.0.0.4', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 5', host: '127.0.0.5', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 6', host: '127.0.0.6', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 7', host: '127.0.0.7', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 8', host: '127.0.0.8', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 9', host: '127.0.0.9', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 10', host: '127.0.0.10', token: '123456'});
    await delay(10); // wait 10 ms
    service.addDevice({ name: 'Simulate Backend 11', host: '127.0.0.11', token: '123456'});
    const devices: DeviceType[] = service.getDevices();
    expect(devices).toEqual([
      { name: 'Simulate Backend 11', host: '127.0.0.11', token: '123456'},
      { name: 'Simulate Backend 10', host: '127.0.0.10', token: '123456'},
      { name: 'Simulate Backend 9', host: '127.0.0.9', token: '123456'},
      { name: 'Simulate Backend 8', host: '127.0.0.8', token: '123456'},
      { name: 'Simulate Backend 7', host: '127.0.0.7', token: '123456'},
      { name: 'Simulate Backend 6', host: '127.0.0.6', token: '123456'},
      { name: 'Simulate Backend 5', host: '127.0.0.5', token: '123456'},
      { name: 'Simulate Backend 4', host: '127.0.0.4', token: '123456'},
      { name: 'Simulate Backend 3', host: '127.0.0.3', token: '123456'},
      { name: 'Simulate Backend 2', host: '127.0.0.2', token: '123456'},
    ]);
  });

});
