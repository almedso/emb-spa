import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { AuthGuardService } from './auth-guard.service';
import { AuthService } from '../services/auth.service';

describe('AuthGuardService', () => {
  let service: AuthGuardService;
  beforeEach(() => {
    const asSpy = jasmine.createSpyObj('AuthService', ['isAuthenticated'] );
    const rSpy = jasmine.createSpyObj('Router', ['navigate']);
    TestBed.configureTestingModule({
      providers: [
        { provide: AuthService, useValue: asSpy },
        { provide: Router, useValue: rSpy },
        AuthGuardService,
      ],
    });
    service = TestBed.inject(AuthGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
