import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestbedComponent } from './testbed/testbed.component';
import { TestSuiteComponent } from './test-suite/test-suite.component';

const routes: Routes = [

  {
    path: 'testbed',
    component: TestbedComponent
  },
  {
    path: 'test-suite',
    component: TestSuiteComponent
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule { }
