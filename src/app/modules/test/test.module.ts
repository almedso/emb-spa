import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRoutingModule } from './test-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { LayoutModule } from '@angular/cdk/layout';

import { SharedModule } from '@/shared/shared.module';
import { TestSuiteComponent } from './test-suite/test-suite.component';
import { TestbedComponent } from './testbed/testbed.component';


@NgModule({
  declarations: [
  TestSuiteComponent,
  TestbedComponent],
  imports: [
    CommonModule,
    TestRoutingModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    SharedModule,
  ]
})
export class TestModule { }
