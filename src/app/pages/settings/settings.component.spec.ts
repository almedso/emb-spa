import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Component } from '@angular/core';

import { SettingsComponent } from './settings.component';
import { SharedModule } from '@/shared/shared.module';
import { LayoutModule } from '@angular/cdk/layout';

@Component({
  template: ''
})
class DummyComponent {
}

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsComponent ],
      imports: [
        LayoutModule,
        SharedModule,
        RouterTestingModule.withRoutes([
          { path: 'settings', component: DummyComponent }
         ]),
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
