import { Component, OnInit, } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { DeviceService } from '@/core';
import { DeviceType } from '@/shared';
import { AuthService } from '@/core';

interface IndexedDeviceType extends DeviceType {
  id: string;
}

const ADD_DEVICE_SELECTOR: DeviceType = {
  name: '*** Add a new device ***',
  host: '',  // hostname is empty to force entering it
  token: ''  // token is empty to force entering it
};

function insert_index(devices: DeviceType[]): IndexedDeviceType[] {
  // tslint:disable-next-line:prefer-const
  let result: IndexedDeviceType[] = [];
  for (const index in devices) {
    if (devices.hasOwnProperty(index)) {
      const device: IndexedDeviceType =  {
        id: index.toString(),
        name: devices[index].name,
        host: devices[index].host,
        token: devices[index].token
      };
      result.push(device);
    }
  }
  return result;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = this.fb.group({
    token: [null, Validators.required],
    host: [null, Validators.required],
    devices: [null, ],
  });

  public devices: IndexedDeviceType[] = [];

  addNewDevice = false;  // show variable
  addToken = false;  // show variable
  loading = false;  // show variable
  submitAllowed = true;  // indicate Submit allowed
  addDeviceSelector = 0; // Index that is assigned to ADD_DEVICE_SELECTOR

  returnUrl: string;  // go there if successful login

  constructor(
    private fb: FormBuilder,
    private deviceService: DeviceService,
    private authenticationService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  loadDevices() {
    // we add the device selector to the end
    // since it is more likely, that something is available already
    // tslint:disable-next-line:prefer-const
    let devices = this.deviceService.getDevices();
    console.log('Load devices: ', devices);
    devices.push(ADD_DEVICE_SELECTOR);
    this.addDeviceSelector = this.devices.length - 1;
    this.devices = insert_index(devices);
  }
  ngOnInit() {
    console.log('Login - nginit');
    // force an automatic logout if this page is served
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';

    // figure out if host and token are already set.
    const host: string = this.route.snapshot.queryParamMap.get('host');
    const token: string = this.route.snapshot.queryParamMap.get('token');
    if ( host ) {
      console.log('Login component - token: ', + token);
      console.log('Login component - host: ', + host);
      this.loginForm.patchValue({ host, token });
      // TODO - ?? perform login here if  both set??
    } else {
      this.loadDevices();
    }
  }

  deviceAvailable(): boolean { return (this.devices.length > 1); }

  onDeviceSelectionChange() {
    // definitely something is selected if this function is called
    const selected = Number(this.loginForm.value.devices);
    console.log('SelectionValue: ', selected );
    if ( selected === this.addDeviceSelector ) {
      this.loginForm.patchValue( { host: '' , token: '' });
    } else {
      this.loginForm.patchValue({
        host: this.devices[selected].host,
        token: this.devices[selected].token
      });
    }
    // show host and token fields if invalid values are set
    this.addNewDevice = this.loginForm.invalid;
    this.addToken = this.loginForm.invalid;
  }

  onSubmit() {
    console.log('Login pressed', this.loginForm.value.devices );
    if ( !this.loginForm.value.devices
    || this.loginForm.value.devices === this.addDeviceSelector ) {
      this.addNewDevice = true;
      this.addToken = true;
    } else {
      // host and token have been passing validation
      // so we can login now.
      this.deviceService.addDevice({
        name: this.loginForm.value.host,  // do something else after query
        host: this.loginForm.value.host,
        token: this.loginForm.value.token
      });
    }

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;
    this.authenticationService.login(this.loginForm.value.host, this.loginForm.value.token)
      .pipe(first())
      .subscribe(
        ( device: DeviceType ) => {
          console.log('Successful login to device: ', device );
          // update the device service
          this.deviceService.addDevice(device);
          this.loading = false;
          this.router.navigate([this.returnUrl]);
        },
         error  => {
          alert('FAILED Login on ' + this.loginForm.value.host + ' using token ' + this.loginForm.value.token);
          this.loading = false;
          this.addNewDevice = true;
          this.addToken = true;
        });

  }
}
