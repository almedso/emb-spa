import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';


export interface Tile {
  cols: number;
  text: string;
  link: string;
  icon: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  tiles: Tile[] = [
    {text: 'Testbed', link: '/esp/testbed', icon: 'play_circle_filled', cols: 1, },
    {text: 'Test Suites', link: '/esp/test-suite', icon: 'play_circle_outline', cols: 1, },
    {text: 'Settings', link: '/settings', icon: 'build', cols: 1, },
    {text: 'Logout', link: '/login', icon: 'build', cols: 1, },
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userIdle: UserIdleService,
  ) { }

  ngOnInit() {

    const host: string = this.route.snapshot.queryParamMap.get('host');
    const accessToken: string = this.route.snapshot.queryParamMap.get('token');
    console.log('token: ', + accessToken);
    console.log('host: ', + host);
    console.log(this.route.snapshot.queryParamMap);
    // Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => console.log(count));

    this.userIdle.onTimeout().subscribe(() => {
      // logout after timeout
      // note that the login component - if called automatically logs out
      this.router.navigate(['/login']);
    });

  }

  stop() {
    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.userIdle.startWatching();
  }

  restart() {
    this.userIdle.resetTimer();
  }
}

