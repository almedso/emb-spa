# OPEN IVD SPA

SPA application for an Open IVD instrument

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

Potentially the  chrome driver needs to be updated. The chrome driver version (major portion) must fit to the chrome version.

Determine the Chrome version by:  Menu -> Help  -> About Chrome. It delivers e.g. 79.0.3945.88.

Update the Webdriver using:

```
node node_modules/protractor/bin/webdriver-manager update --versions.chrome 79.0.3945.36
```

You can verify the driver version via:

```
node node_modules/protractor/bin/webdriver-manager status
```


And call e2e tests without driver update.

```
ng e2e --no-webdriver-update
```


